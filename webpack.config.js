module.exports = (webpackConfig)=> {
  let retVal = Object.assign({}, webpackConfig, {
    externals: {
      'react': 'React',
      'react-dom': 'ReactDOM',
      // 提出ant design的公共资源
      //'antd': 'antd',
    },
    devServer: {
      //host:'192.168.227.86',
      hot: true,
      inline: true,
      port: 9000,
      stats: { colors: true, progress: false },
      compress: true,
      quiet: false,
      clientLogLevel: 'info',
      open: false,
      historyApiFallback: true,
      //host: '192.168.220.4',
      proxy: {
        '/api': {
          changeOrigin: true,
          target: 'http://localhost:3000',
          secure: false
        },
        '/api-cms': {
          changeOrigin: true,
          target: 'http://localhost:3000',
          secure: false
        }
      }
    },
  });
  const svgDirs = [
    require.resolve('antd-mobile').replace(/warn\.js$/, ''),  // 1. 属于 antd-mobile 内置 svg 文件
    // path.resolve(__dirname, 'src/my-project-svg-foler'),  // 2. 自己私人的 svg 存放目录
  ];

  retVal.module.loaders.push({
    test: /\.(svg)$/i,
    loader: 'svg-sprite',
    include: svgDirs  // 把 svgDirs 路径下的所有 svg 文件交给 svg-sprite-loader 插件处理
  });
  retVal.babel.plugins.push(['import', { libraryName: 'antd-mobile', style: 'css' }]);

  return retVal;
};
