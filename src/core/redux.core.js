import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { persistStore, autoRehydrate } from 'redux-persist';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import reducers from '../reducers';

const isDebuggingInChrome = window.navigator.userAgent;

const logger = createLogger({
  predicate: (getState, action) => isDebuggingInChrome,
  collapsed: true,
  duration: true,
});
const reduxMiddleware = routerMiddleware(browserHistory);

const createAppStore = applyMiddleware(thunk, logger, reduxMiddleware)(createStore);


function configureStore(onComplete) {
  // TODO(frantic): reconsider usage of redux-persist, maybe add cache breaker
  // 准备存储
  const store = autoRehydrate()(createAppStore)(reducers);
  // persistStore(store, {
  //   storage: window.sessionStorage,
  //   blacklist: ['goods']
  // }, onComplete);
  if (isDebuggingInChrome) {
    window.store = store;
  }
  return store;
}

export default configureStore;
