

const tmpResult = {
  code: 200,
  dataMap: {
    text: '消息'
  },
  message: ''
};

export const METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'
};

export const CONTENT_TYPE = {
  'x-www-form-urlencoded': 'application/x-www-form-urlencoded',
  formdata: 'application/formdata',
  json: 'application/json'
};

class ModelCore {
  constructor(url) {
    this.url = url;
  }

  /**
   * 执行请求
   *
   * @param {Object} options
   */
  async _execute(options = {}) {
    const url = this.url;
    const _options = Object.assign({}, {
      method: 'GET',
      credentials: 'same-origin',
      headers: {},
      emulateJSON: false,
      showError: true,
      apiPath: '/api'
    }, options);

    if (!_options.headers['Content-Type']) {
      if (options.emulateJSON) {
        const datas = [];
        // convert object to string
        if (options.data instanceof FormData) {
          options.data.forEach((value, key) => {
            datas.push(`${key}=${value}`);
          });
        } else {
          Object.keys(options.data).forEach((key) => {
            datas.push(`${key}=${options.data[key]}`);
          });
        }
        _options.headers['Content-Type'] = CONTENT_TYPE['x-www-form-urlencoded'];
        _options.body = datas.join('&');
      } else if (_options.method == METHOD.POST ||
        _options.method == METHOD.PUT ||
        _options.method == METHOD.DELETE) {
        _options.headers['Content-Type'] = CONTENT_TYPE.json;
        if (_options.data) {
          _options.body = JSON.stringify(options.data);
        }
      }
      delete _options.data;
    }
    if (!_options.apiPath) {
      _options.apiPath = '/api';
    }
    const path = `${_options.apiPath}/${url}`.replace(/\/{2}/g, '/');
    const response = await fetch(path, _options);

    return new Promise((resolve, reject) => {
      fetch(path, _options)
        // .then(checkStatus)
        // .catch((error) => {
        //   // 异常分支
        //   // 触发异常
        //   onfire.fire(Consts.EVENT_KEY.ERROR);
        //   // 触发网络异常
        //   onfire.fire(Consts.EVENT_KEY.NET_COMMUNICATION.ERROR);
        //   // 未认证
        //   if (error.response.status == 401) {
        //     // 触发401异常
        //     onfire.fire(Consts.EVENT_KEY.NET_COMMUNICATION.BUSINESS_ERROR.ERROR_401);
        //     return;
        //   } else {
        //     throw error;
        //   }
        // })
        .catch((err) => {
          reject({
            code: err.response.status
          });
        })
        .then((response) => {

          // response.json()
          // return Promise.resolve()
        })
        .then((data) => {
          // 业务异常 业务code 200 以外
          if (data.code != 200) {
            //   // 触发异常
            //   onfire.fire(Consts.EVENT_KEY.ERROR);
            //   // 触发网络异常
            //   onfire.fire(Consts.EVENT_KEY.NET_COMMUNICATION.ERROR);
            //   // 显示信息
            //   if (_options.showError) {
            //     onfire.fire(Consts.EVENT_KEY.NET_COMMUNICATION.BUSINESS_ERROR.NOT_200, {
            //       code: data.code,
            //       message: data.message || '业务错误请再试试!'
            //     });
            //   }
            reject(data);
          } else {
            resolve(data.dataMap, data, data);
          }
        })
        .catch((error) => {
          // if (error.response) {
          //   onfire.fire(Consts.EVENT_KEY.NET_COMMUNICATION.NORMAL_ERROR.ERROR_500);
          // }
          reject({
            ...error
          });
        });
    });
  }

  /**
   * 查询
   */
  async get() {
    const url = this.url;
    return tmpResult;
  }

  /**
   * 新增
   */
  async post() {
    const url = this.url;
    return tmpResult;
  }

  /**
   * 更新
   */
  async put() {
    const url = this.url;
    return tmpResult;
  }

  /**
   * 删除
   */
  async delete() {
    const url = this.url;
    return tmpResult;
  }


}

export default ModelCore;
