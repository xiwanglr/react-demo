import React, { Component } from 'react';
import { Button, Icon } from 'antd-mobile';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import './style.scss';
import img1 from '../../../imgs/favicon.png';
import { getSampleMessage } from '../../../actions';

//import Affix from 'antd/lib/affix';

//import Button from 'antd-mobile/lib/button';
class Sample extends Component {

  constructor(props) {
    super(props);
    const that = this;
    that.onClickButton = that.onClickButton.bind(that);
    that.onClickImage = that.onClickImage.bind(that);
    that.state = {
      message: ''
    };
  }

  async onClickButton() {
    const that = this;
    const { dispatch } = that.props;
    dispatch(await getSampleMessage());
    // fetch('/api/sample')
    //   .then(response => response.json())
    //   .then((result) => {
    //     that.setState({
    //       message: result.dataMap.text
    //     });
    //   });
  }

  async onClickImage() {
    const that = this;
    const { dispatch } = that.props;
    dispatch(goBack());
  }

  render() {
    console.log('Sapmle class render');
    const that = this;
    return (
      <div className="sample">
        <div className="title">
          例子标题
        </div>
        <div>
          <img src={img1} alt="ff" onClick={that.onClickImage} className="logo" />
          <Icon type="check" />
        </div>
        <div>
          <Button className="btn" icon="check-circle-o" onClick={that.onClickButton}>测试</Button>
          <div className="messageText">
            {that.props.message}
          </div>
        </div>
      </div>
    );
  }
}


const select = (store) => {
  return {
    message: store.sample.message || ''
  };
};

export default connect(select)(Sample);
