import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import {
  Router,
  Route,
  browserHistory
} from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import { Layout1 } from '../../components/layout';
import { chat } from '../../components/chat';
import configureStore from '../../core/redux.core';

require('./style.scss');
require('babel-polyfill');
require('whatwg-fetch');

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.checkAuth = this.checkAuth.bind(this);
    const store = configureStore(() => {
      this.setState({ isLoading: false });
    });
    const reduxHistory = syncHistoryWithStore(browserHistory, store);

    this.state = {
      isLoading: true,
      store,
      reduxHistory
    };
  }

  checkAuth(nextState, replace, cb) {
    console.log('checkAuth');
    // console.log('nextState, replace', nextState, replace);
    // replace('/');
    cb();
  }

  render() {
    console.log('Main render');
    // if (this.state.isLoading) {
    //   return null;
    // }
    return (
      <Provider store={this.state.store}>
        <Router history={browserHistory}>
          <Route path="/" component={chat} onEnter={this.checkAuth}>
            // <Route path="sample" component={require('react-proxy-loader!./sample/Sample')} />
          </Route>
        </Router>
      </Provider>
    );
  }
}

ReactDom.render(
  <Main />,
  document.getElementById('application')
);
