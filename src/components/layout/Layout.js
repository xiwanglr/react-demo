import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

class Layout extends Component {

  constructor(props) {
    super(props);
    this.onClickSampleForward = this.onClickSampleForward.bind(this);
    this.isLoading = true;
  }

  onClickSampleForward() {
    const { dispatch } = this.props;
    dispatch(push('/sample'));
    console.log('onClickSampleForward');
  }

  render() {
    console.log('Layout render');
    const LinkSample =
      this.props.children ? this.props.children
        : <div onClick={this.onClickSampleForward} style={{ fontSize: '48px' }}>Sample</div>;
    return (
      <div style={{ marginTop: '20px', backgroundColor: '#fff' }}>
        {LinkSample}
      </div>
    );
  }
}

export default connect()(Layout);
