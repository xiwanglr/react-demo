
const initialState = {
  message: '默认信息'
};


function sample(state = initialState, action) {
  let result = null;
  switch (action.type) {
    case 'SAMPLE_GET_MESSAGE':
      result = {
        message: action.data
      };
      break;
    default:
      result = state;
  }

  return result;
}

export default sample;
