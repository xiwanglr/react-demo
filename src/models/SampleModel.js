import ModelCore from '../core/model.core';

class SampleModel extends ModelCore {
  query(){
    return super.get();
  }
}

export default SampleModel;
