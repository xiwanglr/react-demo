
/**
 * 调用Sample api
 */
export async function getSampleMessage() {
  const response = await fetch('/api/sample');
  const ret = await response.json();

  return {
    type: 'SAMPLE_GET_MESSAGE',
    data: ret.dataMap.text
  };
}

export default () => { };

